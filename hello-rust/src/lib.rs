use suborbital::runnable::*;
use suborbital::http;
use std::str;


struct HelloRust{}

impl Runnable for HelloRust {
    fn run(&self, input: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        let in_string = String::from_utf8(input).unwrap();
        
        let query_result = match http::get("http://localhost:8080", None) {
            Ok(data) => data,
            Err(e) => e.message.as_bytes().to_vec()
        };

        let s = match str::from_utf8(&query_result) {
            Ok(v) => v,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
        };

        println!("🌍 {}",s);

        Ok(String::from(format!("🦀 hello {}", in_string)).as_bytes().to_vec())
    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &HelloRust = &HelloRust{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
