package main

import (
	"fmt"
	"os"

	"github.com/suborbital/reactr/rt"
	"github.com/suborbital/reactr/rwasm"
)

func main() {
	
	r := rt.New()
	doHelloWasm := r.Register("hello-world", rwasm.NewRunner(os.Args[1]))
	
	result, err := doHelloWasm("Bob Morane!").Then()
	if err != nil {
		fmt.Println(err)
		return
	}
	
	fmt.Println(string(result.([]byte)))

}
