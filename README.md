# demo-reactr-runnables

## Build the Runnable launcher

```bash
go build
```

## Create and build a Runnable

```bash
# create the runnable
subo create runnable hello-go --lang tinygo
# build the runnable
subo build hello-go/
```

```bash
# create the runnable
subo create runnable hello-rust
# build the runnable
subo build hello-rust/
```

### Add http request

```golang
import (
	"log"

	"github.com/suborbital/reactr/api/tinygo/runnable"
	reactrHttp "github.com/suborbital/reactr/api/tinygo/runnable/http"
)

queryResult, _ := reactrHttp.GET("http://localhost:8080", nil)
log.Println(string(queryResult))
```

```rust
use suborbital::runnable::*;
use suborbital::http;
use std::str;



let query_result = match http::get("http://localhost:8080", None) {
    Ok(data) => data,
    Err(e) => e.message.as_bytes().to_vec()
};

let s = match str::from_utf8(&query_result) {
    Ok(v) => v,
    Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
};

println!("🌍 {}",s);
```