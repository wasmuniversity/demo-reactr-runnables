import Suborbital

class HelloSwift: Suborbital.Runnable {
    func run(input: String) -> String {
        let queryResult: String = HttpGet(url: "http://localhost:8080")
        print(queryResult)
        return "👋 hello " + input + " from Swift 😃"
    }
}

Suborbital.Set(runnable: HelloSwift())
